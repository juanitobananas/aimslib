package com.jarsilio.android.scrambler.utils

import java.lang.StringBuilder

internal object Utils {
    fun byteArrayFromInts(vararg ints: Int) = ByteArray(ints.size) { pos -> ints[pos].toByte() }

    fun ByteArray.toHexString(): String {
        val stringBuilder = StringBuilder()

        for (byte in this) {
            stringBuilder.append(byte.toHexString())
        }

        return stringBuilder.toString()
    }

    fun Byte.toHexString(): String {
        return "%02X".format(this)
    }
}
