package com.jarsilio.android.scrambler

import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertEquals
import org.junit.Test
import java.io.File

/**
 * The picture used in these tests is of the great Marvin.
 * Taken with ❤️ by Pututu. Thank you very much for that!
 */
class ImageTypeUnitTest {
    @Test
    fun loadTestJpegFromResources() {
        val jpegImage = File(javaClass.getResource("/marvincito.jpg")!!.path)
        assertThat(jpegImage, notNullValue())
    }

    @Test
    fun testGetImageTypeJpeg() {
        val jpegImage = File(javaClass.getResource("/marvincito.jpg")!!.path)
        val imageType = getImageType(jpegImage)
        assertEquals(imageType, ImageType.JPG)
    }

    @Test
    fun testGetImageTypeUnknown() {
        val file = File(javaClass.getResource("/helloworld.txt")!!.path)
        val imageType = getImageType(file)
        assertEquals(imageType, ImageType.UNKNOWN)
    }
}
